import gql from 'graphql-tag';

export const FEATCH_SELLER = gql`
    query searchSeller($search: String){
        searchSeller(search: $search){
            _id
            businessName
            initDate
            endDate
            photo_url
        }
    }
`;

export const FEATCH_CATEGORIES_SELLER = gql`
    query getCategoriesSeller($_id: String!){
        getCategoriesSeller(_id: $_id){
            _id
            photo_url
            name
            description
        }
    }
`;

export const FEATCH_PRODUCT_CATEGORIE = gql`
    query getProductsCategories($_id: String!){
        getProductsCategories(_id: $_id){
            _id
            photo_url
            name
            stoktaking
            price
            visible
            category{
                name
            }
            seller{
                _id
                businessName
            }
        }
    }
`;

export const FEATCH_USER = gql`
    {
        getBuyer{
            _id
            name
            lastname
            email
            photo_url
            physicalAddress
            mobile
            phone
            address {
                lat
                lng
                zoom
            }
        }
    }
`;