import React, { Component } from 'react';

import { View, Image, TouchableNativeFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

export default class ShoppinCartButton extends Component {
  render() {
    return (
        <TouchableNativeFeedback {...this.props} >
            <View style={{ flexDirection: 'row' }} >
                <Icon
                    name='shoppingcart'
                    size={30}
                    color="#fff"
                    style={{marginRight: 19}}
                />
            </View>
        </TouchableNativeFeedback>
    );
  }
}