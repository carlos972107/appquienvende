import { DefaultTheme } from 'react-native-paper';

export const theme = {
  colors: {
    primary: '#423089',
    secondary: '#EF7C21',
    error: '#f13a59',
  },
};