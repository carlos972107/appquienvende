import {combineReducers} from 'redux';
import AuthReducer from './authReducer';
import ShoppingCartReducer from './shoppingCartReducer';

export default combineReducers({
    AuthReducer,
    ShoppingCartReducer
});