export const login = (stateAuth) => ({
    type: 'LOGIN',
    stateAuth,
});

export const addSession = (user) => ({
    type: 'ADD_SESSION',
    user,
});