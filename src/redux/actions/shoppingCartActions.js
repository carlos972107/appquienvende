export const addProduct = (product, seller) => ({
    type: 'ADD_PRODUCT',
    product,
    seller,
});

export const decProduct = (product, seller) => ({
    type: 'DEC_PRODUCT',
    product,
    seller,
});

export const popProduct = (_id) => ({
    type: 'POP_PRODUCT',
    _id
});

export const filtProduct = (_id) => ({
    type: 'FILT_PRODUCT',
    _id
})