import { StyleSheet, Dimensions } from 'react-native';

// screen sizing
const { width, height } = Dimensions.get('window');
// orientation must fixed
const SCREEN_WIDTH = width < height ? width : height;

// item size
const RECIPE_ITEM_HEIGHT = 150;
const RECIPE_ITEM_MARGIN = 10;

// 2 photos per width
export const RecipeCard = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: RECIPE_ITEM_MARGIN,
    marginTop: 10,
    marginBottom: 20,
    width: SCREEN_WIDTH - 20,
    height: RECIPE_ITEM_HEIGHT + 60,
    borderColor: '#cccccc',
    borderWidth: 0.5,
    borderRadius: 15,
    backgroundColor: '#fff'
  },
  photo: {
    width: SCREEN_WIDTH - 20,
    height: RECIPE_ITEM_HEIGHT,
    borderRadius: 15,
    borderWidth: 0.1,
    borderColor: '#000',
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0
  },
  title: {
    flex: 1,
    fontSize: 20,
    fontWeight: 'bold',
    color: '#444444',
    marginTop: 4,
    marginRight: 5,
    marginLeft: 15,
  },
  description: {
    flex: 1,
    fontSize: 15,
    color: '#444444',
    marginRight: 5,
    marginLeft: 15,
  }
});