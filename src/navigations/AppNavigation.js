import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {createStackNavigator} from 'react-navigation-stack';

import LoginScreen from '../screens/Login/LoginScreen';
import RegisterScreen from '../screens/Register/RegisterScreen';
import ForgotPasswordScreen from '../screens/ForgotPassword/ForgotPasswordScreen';
import SellerScreen from '../screens/Seller/SellerScreen';
import ProductScreen from '../screens/Product/ProductScreen';
import CategoriesListScreen from '../screens/CategoriesList/CategoriesListScreen';
import ProductListScreen from '../screens/ProductList/ProductListScreen';
import DrawerContainer from '../screens/DrawerContainer/DrawerContainer';
import ShoppinCartScreen from '../screens/ShoppinCart/ShoppinCartScreen';
import SplashScreen from '../screens/Splash/SplashScreen';
import SearchScreen from '../screens/Search/SearchScreen';
import RequestOrdersScreen from '../screens/RequestOrders/RequestOrders';
import ProfileScreen from '../screens/Profile/ProfileScreen';
import ChangePasswordScreen from '../screens/ChangePassword/ChangePasswordScreen';

const Token = localStorage.getItem('mobile-token-authorization');

const MainNavigator = createStackNavigator(
  {
    Seller: SellerScreen,
    ShoppinCart: ShoppinCartScreen,
    Product: ProductScreen,
    ChangePassword: ChangePasswordScreen,
    CategoriesList: CategoriesListScreen,
    Profile: ProfileScreen,
    ProductList: ProductListScreen,
    Search: SearchScreen,
    RequestOrders: RequestOrdersScreen
  },
  {
    defaulfNavigationOptions: ({ navigation }) => ({
      headerTitleStyle: {
        fontWeight: 'bold',
        textAlign: 'center',
        alignSelf: 'center',
        flex: 1
      }
    })
  }
);

const RootNavigator = createStackNavigator(
  {
    Splash: SplashScreen,
    Login: LoginScreen,
    ForgotPassword: ForgotPasswordScreen,
    Register: RegisterScreen,
  }, {
    headerMode: 'none'
  }
)

const DrawerStack = createDrawerNavigator(
  {
    Main: MainNavigator
  },
  {
    drawerPosition: 'left',
    initialRouteName: 'Main',
    drawerWidth: 300,
    contentComponent: DrawerContainer
  }
);

const SwitchNavigator = createSwitchNavigator(
  {
    RootNavigator: RootNavigator,
    DrawerStack: DrawerStack
  },
  {
    initialRouteName: !Token ? 'RootNavigator' : 'DrawerStack'
  }
);


export default AppContainer = createAppContainer(SwitchNavigator);

console.disableYellowBox = true;