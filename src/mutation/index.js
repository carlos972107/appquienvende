import { gql } from '@apollo/client';


export const TOKEN_NOTIFICATION = gql`
    mutation createTokenNotification($token: String!){
        createTokenNotification(token: $token){
            _id
        }
    }
`;

export const LOGIN_BUYER = gql`
    mutation loginBuyer($email: String!, $password: String!){
        loginBuyer(email: $email, password: $password)
    }
`;

export const UPDATE_PASSWORD_BUYER = gql`
    mutation updatePasswordBuyer($password: String!){
        updatePasswordBuyer(password: $password){
            _id
        }
    }
`;

export const FORGOT_PASSWORD_BUYER = gql`
    mutation forgotPasswordBuyer($email: String!){
        forgotPasswordBuyer(email: $email){
            _id
        }
    }
`;

export const CREATE_BUYER = gql`
    mutation createBuyer($name: String!, $lastname: String!, $phone: String!, $mobile: String!, $email: String!, $password: String!, $address: AddressInput!, $photo_url: String, $role: String!, $state: String!, $physicalAddress: String!){
        createBuyer(data: { name: $name, lastname: $lastname, phone: $phone, mobile: $mobile, email: $email, password: $password, address: $address, photo_url: $photo_url, role: $role, state: $state, physicalAddress: $physicalAddress }){
            _id
        }
    }
`;

export const UPDATE_BUYER = gql`
    mutation updateBuyer($name: String!, $lastname: String!, $physicalAddress: String!, $phone: String!, $mobile: String!, $email: String!, $photo_url: String, $_id: String!){
        updateBuyer(data: { name: $name, lastname: $lastname, physicalAddress: $physicalAddress, phone: $phone, mobile: $mobile, email: $email, photo_url: $photo_url }, _id: $_id){
            _id
        }
    }
`;