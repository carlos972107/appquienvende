'use strict';

import { MaskService } from 'react-native-masked-text';

export const formatPrice = (value) => {
    return MaskService.toMask('money', value, {
        unit: '',
        delimiter: ',',
        precision: 0
    });
};

export const validInputValue = (inputValue) => {
    if(inputValue.length <= 0){
        return false;
    }
    return true;
}

export const validInputEmail = (inputValue) => {
    const valid = /\S+@\S+\.\S+/;
    if(inputValue.length <= 0 && !valid.test(inputValue)){
        return false;
    }
    return true;
}

export const emailValidatorLogin = (email) => {
    const re = /\S+@\S+\.\S+/;
    if (!email || email.length <= 0) return 'El campo no puede quedar vacio.';
    if (!re.test(email)) return 'Correo electronico no valido.';
    return '';
};