import React from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    Alert,
    StyleSheet,
} from 'react-native';
import { TextInput as Input, Button as PaperButton } from 'react-native-paper';
//import Geocoder from 'react-native-geocoding';
import Loader from '../../components/Loader/Loader';
import ContentRoot from '../../components/ContentRoot/ContentRoot';
import { theme } from '../../theme';
import { useMutation } from '@apollo/react-hooks';
import { CREATE_BUYER } from '../../mutation';

//Geocoder.init("AIzaSyBLAo5oS5bU3mXvZ4x6yNkU_t4tBDiL-qc", {language : "en"});

const RegisterScreen = ({navigation}) => {

    const [name, setName] = React.useState({ value: '' });
    const [lastname, setLasname] = React.useState({ value: '' });
    const [phone, setPhone] = React.useState({ value: '' });
    const [mobile, setMobile] = React.useState({ value: '' });
    const [password, setPassword] = React.useState({ value: '' });
    const [email, setEmail] = React.useState({ value: '' });
    const [address, setAddress] = React.useState({ lat: '', lng: '', zoom: 0 });
    const [physicalAddress, setPhysicalAddress] = React.useState({ value: '' });
    const [loader, setLoader] = React.useState({ value: false });

    const [createBuyer, { data }] = useMutation(CREATE_BUYER);

    const encontrarCoordenadas = () => {
        navigator.geolocation.getCurrentPosition(
            posicion => {
            const coordenadas = posicion;
            setAddress({
                lat: coordenadas.coords.latitude.toString(),
                lng: coordenadas.coords.longitude.toString(),
                zoom: 12
            });
            //goggle maps hay que comprar el uso de la api
            /*Geocoder.from(coordenadas.coords.latitude, coordenadas.coords.longitude).then(location => {
                console.log(location)
            });*/
          },
          error => console.log(error.message),
          { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
    };

    encontrarCoordenadas();

    const handleOnPressRegister = async () => {
        setLoader({ value: true });
        let { data } = await createBuyer({ variables: { 
            name: name.value,
            lastname: lastname.value,
            phone: phone.value,
            mobile: mobile.value,
            email: email.value,
            password: password.value,
            address,
            physicalAddress: physicalAddress.value,
            role: 'BUYER',
            state: 'VERIFICACION',
            photo_url: ''
        }}).catch(error => {
            setLoader({ value: false });
            Alert.alert('Error!', 'Ocurrio algo inesperado.', [
                {text: 'Ok'}
            ]);
            console.log(error);
        });
        if(data.createBuyer){
            setLoader({ value: false });
            Alert.alert('Creado con exito!', 'Ya puedes iniciar sesion.', [
                {text: 'Ok'}
            ]);
            navigation.goBack();
            return;
        }
    }

    return (
        <ContentRoot textHeader='Formulario de registro'>
            {loader.value &&
                <Loader />
            }
            <Input
                style={styles.input}
                selectionColor={''}
                theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent'}}}
                mode="outlined"
                label="Nombre"
                returnKeyType="done"
                value={name.value}
                disabled={loader.value}
                onChangeText={text => setName({ value: text })}
            />

            <Input
                style={styles.input}
                selectionColor={''}
                theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                mode="outlined"
                label="Apellido"
                returnKeyType="done"
                value={lastname.value}
                disabled={loader.value}
                onChangeText={text => setLasname({ value: text })}
            />

            <Input
                style={styles.input}
                selectionColor={''}
                theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                mode="outlined"
                label="Correo electronico"
                keyboardType='email-address'
                returnKeyType="done"
                value={email.value}
                disabled={loader.value}
                onChangeText={text => setEmail({ value: text })}
            />

            <Input
                style={styles.input}
                selectionColor={''}
                theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                mode="outlined"
                label="Telefono"
                keyboardType='numeric'
                returnKeyType="done"
                value={phone.value}
                disabled={loader.value}
                onChangeText={text => setPhone({ value: text })}
            />

            <Input
                style={styles.input}
                selectionColor={''}
                theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                mode="outlined"
                label="Celular"
                keyboardType='numeric'
                returnKeyType="done"
                value={mobile.value}
                disabled={loader.value}
                onChangeText={text => setMobile({ value: text })}
            />

            <Input
                style={[styles.input, { marginTop: 3 }]}
                selectionColor={''}
                mode="outlined"
                label="Contraseña"
                theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                returnKeyType="done"
                value={password.value}
                onChangeText={text => setPassword({ value: text })}
                disabled={loader.value}
                secureTextEntry
            />

            <Input
                style={styles.input}
                selectionColor={''}
                theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                mode="outlined"
                label="Dirección Fisica"
                keyboardType='text'
                returnKeyType="done"
                value={physicalAddress.value}
                onChangeText={text => setPhysicalAddress({ value: text })}
                disabled={loader.value}
            />

            <PaperButton
                style={[
                    {
                        width: '100%',
                        marginVertical: 10,
                        backgroundColor: theme.colors.primary
                    }
                ]}
                labelStyle={styles.text}
                mode='contained'
                onPress={() => handleOnPressRegister()}
                disabled={loader.value}
            >
                Registrar
            </PaperButton>

            <View style={styles.row}>
                <Text style={styles.label}>Ya tienes una cuenta? </Text>
                <TouchableOpacity onPress={() => navigation.navigate('Login')} disabled={loader.value}>
                    <Text style={styles.link}>Iniciar sesion</Text>
                </TouchableOpacity>
            </View>
        </ContentRoot>
    );
};

RegisterScreen.navigationOptions = {
    headerShown: false
}

export default RegisterScreen;

const styles = StyleSheet.create({
    label: {
        color: theme.colors.secondary,
    },
    button: {
        marginTop: 24,
    },
    row: {
        flexDirection: 'row',
        marginTop: 4,
    },
    link: {
        fontWeight: 'bold',
        color: theme.colors.primary,
    },
    input: {
        width: '100%',
        paddingBottom: 8,
        backgroundColor: '#fff',
        borderBottomColor: '#000'
    },
});