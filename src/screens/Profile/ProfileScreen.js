import React, { useEffect } from 'react';
import {
    Alert,
    StyleSheet,
    TouchableHighlight,
    View
} from 'react-native';
import { TextInput as Input, Button as PaperButton, Avatar } from 'react-native-paper';
import Loader from '../../components/Loader/Loader';
import { theme } from '../../theme';
import { useMutation, useLazyQuery } from '@apollo/react-hooks';
import { UPDATE_BUYER } from '../../mutation';
import { firebase } from '../../firebase/firebase';
import { FEATCH_USER } from '../../query';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';

const ProfileScreen = (props) => {

    const [name, setName] = React.useState({ value: '' });
    const [lastname, setLasname] = React.useState({ value: '' });
    const [phone, setPhone] = React.useState({ value: '' });
    const [mobile, setMobile] = React.useState({ value: '' });
    const [email, setEmail] = React.useState({ value: '' });
    const [loader, setLoader] = React.useState({ value: false });
    const [photo_url, setPhoto] = React.useState({ value: '', prevValue: '' });
    const [address, setAddress] = React.useState({ value: '' });
    const [_id, setId] = React.useState({value: ''})

    const [updateBuyer, {data}] = useMutation(UPDATE_BUYER);
    
    const [getBuyer] = useLazyQuery(FEATCH_USER, {
        onCompleted: data => {
            const { getBuyer } = data;
            setId({value: getBuyer._id});
            setEmail({value: getBuyer.email});
            setLasname({ value: getBuyer.lastname });
            setName({ value: getBuyer.name });
            setPhoto({ value: getBuyer.photo_url, prevValue: getBuyer.photo_url });
            setMobile({ value: getBuyer.mobile });
            setPhone({ value: getBuyer.phone });
            setAddress({ value: getBuyer.physicalAddress });
        }
    });

    useEffect(() => {
        const featchBuyer = async () => {
            return await getBuyer({});
        }
        featchBuyer();
    }, []);

    const handlerOnPressMod = async () => {
        setLoader({ value: true });
        let photoSave = '';
        if(photo_url.value !== photo_url.prevValue 
            && photo_url.prevValue && photo_url.value){
            await deleteStorage(photo_url.preValue);
            photoSave = await saveStorage(photo_url.value); 
        }else if(photo_url.value){
            photoSave = await saveStorage(photo_url.value); 
        }
        let { data } = await updateBuyer({ variables: { 
            name: name.value,
            lastname: lastname.value,
            phone: phone.value,
            mobile: mobile.value,
            email: email.value,
            photo_url: photoSave,
            physicalAddress: address.value,
            _id: _id.value
        }}).catch(async error => {
            setLoader({ value: false });
            Alert.alert('Error!', 'Ocurrio algo inesperado.', [
                {text: 'Ok'}
            ]);
            if(photoSave.length)
                await deleteStorage(photoSave);
            console.log(error);
        });
        if(data.updateBuyer){
            setLoader({ value: false });
            Alert.alert('Modificado!', 'Los datos fueron modificados.', [
                {text: 'Ok'}
            ]);
            return;
        }
    }

    const openGalery = async () => {
        const resultPermission = await Permissions.askAsync(
          Permissions.CAMERA_ROLL
        );
        if(resultPermission){
          const resultImagePicker = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [4, 3]
          });
          if(resultImagePicker.cancelled === false){
            const imageUri = resultImagePicker.uri;
            setPhoto({ value: imageUri });
          }
        }
    }
    
    const uploadImage = uri => {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.onerror = reject;
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    resolve(xhr.response);
                }
            };

            xhr.open("GET", uri);
            xhr.responseType = "blob";
            xhr.send();
        });
    };
    
    const generateUUID = () => {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    }
    
    const saveStorage = async (imageUri) => {
        const refStorage = firebase.storage().ref();
        return uploadImage(imageUri)
        .then(async res => {
          const task = await refStorage.child(`profile/${generateUUID() + new Date()}`).put(res);
          return await task.ref.getDownloadURL();
        }).catch(error => {
          console.log(error);
          return false;
        });
    }
    
    const deleteStorage = async (refUrl) => {
        const refStorage = firebase.storage().refFromURL(refUrl);
        await refStorage.delete();
    }

    return (
        <View style={styles.background}>
            <View style={styles.container}>
                {loader.value &&
                    <Loader />
                }
                <TouchableHighlight underlayColor='rgba(73,182,77,1, 1)' onPress={() => openGalery()}>
                    <Avatar.Image style={{marginBottom: 15, backgroundColor: 'transparent'}} size={150} source={ photo_url.value ? { uri: photo_url.value } : require('../../assets/users_default.png') }  />
                </TouchableHighlight>
                <Input
                    style={styles.input}
                    selectionColor={''}
                    theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent'}}}
                    mode="outlined"
                    label="Nombre"
                    returnKeyType="done"
                    value={name.value}
                    onChangeText={text => setName({ value: text })}
                />

                <Input
                    style={styles.input}
                    selectionColor={''}
                    theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                    mode="outlined"
                    label="Apellido"
                    returnKeyType="done"
                    value={lastname.value}
                    onChangeText={text => setLasname({ value: text })}
                />

                <Input
                    style={styles.input}
                    selectionColor={''}
                    theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                    mode="outlined"
                    label="Correo electronico"
                    keyboardType='email-address'
                    returnKeyType="done"
                    value={email.value}
                    onChangeText={text => setEmail({ value: text })}
                />

                <Input
                    style={styles.input}
                    selectionColor={''}
                    theme={{ colors: { primary: theme.colors.primary, underlineColor:'transparent' }}}
                    mode="outlined"
                    label="Dirección"
                    returnKeyType="done"
                    value={address.value}
                    onChangeText={text => setAddress({ value: text })}
                />

                <Input
                    style={styles.input}
                    selectionColor={''}
                    theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                    mode="outlined"
                    label="Telefono"
                    keyboardType='numeric'
                    returnKeyType="done"
                    value={phone.value}
                    onChangeText={text => setPhone({ value: text })}
                />

                <Input
                    style={styles.input}
                    selectionColor={''}
                    theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                    mode="outlined"
                    label="Celular"
                    keyboardType='numeric'
                    returnKeyType="done"
                    value={mobile.value}
                    onChangeText={text => setMobile({ value: text })}
                />

                <PaperButton
                    style={[
                        {
                            width: '100%',
                            marginVertical: 10,
                            backgroundColor: theme.colors.primary
                        }
                    ]}
                    labelStyle={styles.text}
                    mode='contained'
                    onPress={() => handlerOnPressMod()}
                    disabled={loader.value}
                >
                    Modificar
                </PaperButton>
            </View>
        </View>
    );
}

ProfileScreen.navigationOptions = {
    title: "¿QUIÉN VENDE?, PERFIL",
    headerStyle: {
        backgroundColor: theme.colors.primary,
    },
    headerTintColor: '#fff',
};

export default ProfileScreen;

const styles = StyleSheet.create({
    label: {
        color: theme.colors.secondary,
    },
    button: {
        marginTop: 24,
    },
    row: {
        flexDirection: 'row',
        marginTop: 4,
    },
    link: {
        fontWeight: 'bold',
        color: theme.colors.primary,
    },
    input: {
        width: '100%',
        paddingBottom: 8,
        backgroundColor: '#fff',
        borderBottomColor: '#000'
    },
    background: {
        flex: 1,
        backgroundColor: '#fff',
    },
    container: {
        flex: 1,
        padding: 20,
        width: '100%',
        maxWidth: 340,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
});