import React, { Component, useCallback } from 'react';
import { View, FlatList, ScrollView, Text } from 'react-native';
import { Button, Card, Title, Subheading, Paragraph, TextInput as Input } from 'react-native-paper';
import MapView, { Marker } from 'react-native-maps';
import Select2 from 'react-native-select-two';
import { connect } from 'react-redux';
import { formatPrice } from '../../utils';
import styles from './styles';
import { theme } from '../../theme';

const RenderCard = ({item}) => {
    if(item.quantity !== 0)
    return (
        <Card style={{ borderWidth: 0.2 }}>
            <View style={{flexDirection: 'row'}}>
                <Card.Cover source={{ uri: item.product.photo_url }} style={{height: 80, width: 80, margin: 10}} />
                <Card.Content>
                    <Title numberOfLines={1} style={{width: 190}}>{item.product.name}</Title>
                    <Paragraph>Valor: {formatPrice(item.product.price)} / und</Paragraph>
                    <Paragraph>Total: {formatPrice(item.product.price * item.quantity)}</Paragraph>
                    <Paragraph>Cantidad: {item.quantity}</Paragraph>
                </Card.Content>
            </View>
        </Card>
    )
    return <Text />
}

const ShoppinCartScreen = (props) => {
    
    const [products, setProducts] = React.useState([ ...props.products ]);
    const [paymentMethod, setPaymentMethod] = React.useState('');
    const [stateRef, setState] = React.useState('VERIFICACION');
    let session = { ...props.session };

    const calculPriceTotal = (products) => {
        let total = 0;
        if(products){
            products.map(product => {
                total += product.product.price * product.quantity;
            })
        }
        return total;
    }

    const countProduct = (products) => {
        let count = 0;
        if(products){
            products.map(product => {
                count += product.quantity;
            })
        }
        return count + '';
    }

    const { seller } = props;
    const paymentMethods = [{name: 'CONTRA-ENTREGA', id: 0}];

    return(
        <ScrollView showsVerticalScrollIndicator={false}>
        <Card style={{ margin: 15 }}>
            <Card.Content>
                <Input
                    style={{marginBottom: 15}}
                    selectionColor={''}
                    theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent'}}}
                    mode="outlined"
                    label="Valor total"
                    editable={false}
                    returnKeyType="done"
                    value={formatPrice(calculPriceTotal(products))}
                />
                <Input
                    style={{marginBottom: 15}}
                    selectionColor={''}
                    theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent'}}}
                    mode="outlined"
                    label="Numero de productos"
                    editable={false}
                    returnKeyType="done"
                    value={countProduct(products)}
                />
                <Input
                    style={{marginBottom: 15}}
                    selectionColor={''}
                    theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent'}}}
                    mode="outlined"
                    label="Tienda"
                    editable={false}
                    returnKeyType="done"
                    value={seller.businessName || ''}
                />
                <FlatList
                    vertical
                    style={{marginBottom: 40}}
                    showsVerticalScrollIndicator={false}
                    data={products}
                    renderItem={({item}) => <RenderCard item={item} />}
                    keyExtractor={item => `${item.product._id}`}
                />
                <Input
                    style={{marginBottom: 20}}
                    mode='outlined'
                    render={() => (
                        <Select2
                            isSelectSingle
                            style={{ borderRadius: 4, marginTop: 4, borderColor: 'transparent' }}
                            popupTitle="Metodos de pago"
                            title={paymentMethod || 'Metodos de pago'}
                            searchPlaceHolderText='Metodos de pago'
                            colorTheme={theme.colors.primary}
                            cancelButtonText='Cancelar'
                            selectButtonText='Guardar'
                            value={paymentMethod}
                            data={paymentMethods}
                            onSelect={(id, item) => {
                                setPaymentMethod(item[0].name);
                            }}
                            onRemoveItem={res => {
                                console.log(res[0])
                            }}
                        />
                    )}
                />
                <Input
                    style={{marginBottom: 15}}
                    selectionColor={''}
                    theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent'}}}
                    mode="outlined"
                    label="Estado"
                    editable={false}
                    returnKeyType="done"
                    value={stateRef}
                />
                <Title>Mi Ubicación:</Title>
                <MapView
                    style={{width: '100%', height: 200, marginBottom: 15}}
                    initialRegion={{
                        latitude:10.8488473, 
                        longitude:-74.7751451,
                        latitudeDelta: 0.0010,
                        longitudeDelta: 0.0010,
                    }}
                >
                    <Marker
                        coordinate={{latitude: parseFloat(session.address.lat), longitude: parseFloat(session.address.lng)}}
                        title={session.name + ' ' + session.lastname}
                    />
                </MapView>
                <Input
                    style={{marginBottom: 15}}
                    selectionColor={''}
                    theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent'}}}
                    mode="outlined"
                    label="Direccion fisica"
                    editable={false}
                    returnKeyType="done"
                    value={session.physicalAddress}
                />
                <Button 
                    onPress={()=>{}}
                    mode='contained' 
                    style={{backgroundColor: theme.colors.primary, marginTop: 5}}
                >
                    Realizar compra
                </Button>
            </Card.Content>
        </Card>
        </ScrollView>
    )
}

ShoppinCartScreen.navigationOptions = {
    title: '¿QUIÉN VENDE?, CARRITO DE COMPRAS',
    headerStyle: {
        backgroundColor: theme.colors.primary,
    },
    headerTintColor: '#fff',
};

const mapStateToProps = (state) => {
    return {
      products: state.ShoppingCartReducer.products,
      seller: state.ShoppingCartReducer.seller,
      filter: state.ShoppingCartReducer.filter,
      session: state.AuthReducer.session,
    };
};

export default connect(mapStateToProps, null)(ShoppinCartScreen);