import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  sellerItemContainer: {
    flex: 1,
    margin: 10,
    marginTop: 15,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    height: 230,
    borderColor: '#cccccc',
    borderWidth: 0.5,
    borderRadius: 20,
  },
  sellerPhoto: {
    width: '100%',
    height: 155,
    borderRadius: 20,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    shadowColor: 'blue',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 1.0,
    elevation: 3
  },
  sellerName: {
    flex: 1,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'right',
    color: '#333333',
    marginTop: 8
  },
  sellerInfo: {
    fontSize: 12,
    fontWeight: 'bold',
    marginTop: 2,
    marginBottom: 10
  },
  sellerAddress: {
    marginBottom: 10
  }
});

export default styles;
