import React, { useEffect } from 'react';
import {
  FlatList,
  Text,
  View,
} from 'react-native';
import { Card, Title, Paragraph, Subheading, Searchbar } from 'react-native-paper';
import MenuImage from '../../components/MenuImage/MenuImage';
import ShoppinCartButton from '../../components/ShoppinCartButton/ShoppingCartButton';
import Loader from '../../components/Loader/Loader';
import { theme } from '../../theme';
import { FEATCH_SELLER } from '../../query';
import { TOKEN_NOTIFICATION } from '../../mutation';
import { Query } from '@apollo/react-components';
import { useMutation } from '@apollo/react-hooks';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';

const SellerScreen = (props) => {

  const [createTokenNotification, { data }] = useMutation(TOKEN_NOTIFICATION);
  const [filter, setFilter] = React.useState('');

  const permissionAppPushNotification = async () => {
    const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      return;
    }
    const token = await Notifications.getExpoPushTokenAsync();
    const { data } = await createTokenNotification({ variables: { token } });
  }

  useEffect(()=> {
    async function permission(){
      props.navigation.setParams({ onPressShoppinCart: onPressShoppinCart });
      await permissionAppPushNotification();
    }
    permission();
  }, [])
    
  const onPressShoppinCart = () => {
    props.navigation.navigate('ShoppinCart');
  }

  const onPressSeller = item => {
    const title = item.businessName;
    const sellerId = item._id;
    props.navigation.navigate('CategoriesList', { sellerId, title });
  };

  const renderSeller = ({ item }) => (
    <Card onPress={() => onPressSeller(item)} style={{margin: 15}}>
      <Card.Cover source={item.photo_url ? { uri: item.photo_url } : require('../../../assets/project/no-image-seller.jpg')} />
      <Card.Content style={{alignContent: 'center', alignItems: 'center'}}>
        <Title>{item.businessName}</Title>
        <Paragraph>{'direccion : calle 10'}</Paragraph>
        <View style={{flexDirection: 'row'}}>
          <Subheading>{item.initDate} - </Subheading>
          <Subheading>{item.endDate}</Subheading>
        </View>
      </Card.Content>
    </Card>
  );

    return (
      <View>
        <Searchbar onChange={(text) => setFilter(text.nativeEvent.text) } placeholder="Buscar..." value={filter} style={{margin: 15}}  />
        <Query query={FEATCH_SELLER} pollInterval={5000} variables={{ search: filter }}>
          {({ loading, error, data }) => {
            if(loading) return <Loader/>
            if(data && data.searchSeller.length){
              return <FlatList
                vertical
                showsVerticalScrollIndicator={false}
                data={data.searchSeller}
                renderItem={renderSeller}
                keyExtractor={item => `${item._id}`}
              />
            }else{
              return <Text style={{margin: 15, fontSize: 25, textAlign: 'center', fontWeight: 'bold'}}>
                  Sin datos que mostrar...
                </Text>
            }
          }}
        </Query>
      </View>
    );
}

SellerScreen.navigationOptions = ({ navigation }) =>({
  title: '¿QUIEN VENDE?, TIENDAS',
  headerStyle: {
    backgroundColor: theme.colors.primary,
  },
  headerTintColor: '#fff',
  headerLeft: (
    <MenuImage
      onPress={() => {
        navigation.openDrawer();
      }}
    />
  ),
  headerRight: () => (
    <ShoppinCartButton onPress={navigation.getParam('onPressShoppinCart')} />
  )
});

export default SellerScreen;
