import React from 'react';
import {
  FlatList,
} from 'react-native';
import styles from './styles';
import { Button, Card, Title, Paragraph, Subheading } from 'react-native-paper';
import { View } from 'native-base';
import { theme } from '../../theme';

export default class RequestOrdersScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "¿QUIÉN VENDE?, PEDIDOS",
      headerStyle: {
        backgroundColor: theme.colors.primary,
      },
      headerTintColor: '#fff',
    };
  };

  constructor(props) {
    super(props);
  }

  onPressRequestOrder = item => {
    
  };

  renderRequestOrders = ({ item }) => (
    <Card style={{marginTop: 12}}>
      <View style={{flexDirection: 'row', margin: 12}}>
        <Card.Cover style={{height: 80, width: 80, borderRadius: 12}} source={{ uri: 'https://picsum.photos/700' }} />
        <Card.Content>
          <Title>Pedido #123</Title>
          <Subheading>Tienda: Mercurio Shop</Subheading>
          <Paragraph style={{color: theme.colors.secondary}}>Pedido Alistado</Paragraph>
        </Card.Content>
      </View>
      <Card.Actions style={{flexDirection: 'row-reverse', marginTop: -35}}>
        <Button>Ver detalles</Button>
      </Card.Actions>
    </Card>
  );

  render() {
    return (
      <View style={{margin: 20}}>
        <FlatList
          vertical
          showsVerticalScrollIndicator={false}
          data={[{name: 'Prueba', full_name: 'Full Name', score: '123456'}, {name: 'Prueba', full_name: 'Full Name', score: '123456'}]}
          renderItem={this.renderRequestOrders}
          keyExtractor={item => `${item.recipeId}`}
        />
      </View>
    );
  }
}
