import React from 'react';
import {
  FlatList,
  Text,
  View,
  Image,
} from 'react-native';
import { Card, Title, Subheading, Paragraph } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './styles';
import ShoppinCartButton from '../../components/ShoppinCartButton/ShoppingCartButton';
import ViewButtonAddShoppin from '../../components/ViewButtonAddShoppin/ViewButtonAddShoppin';
import Loader from '../../components/Loader/Loader';
import { FEATCH_PRODUCT_CATEGORIE } from '../../query';
import { Query } from '@apollo/react-components';
import { connect } from 'react-redux';
import { addProduct, decProduct } from '../../redux/actions';
import { filtProduct } from '../../redux/actions/shoppingCartActions';
import { theme } from '../../theme';
import { formatPrice } from '../../utils';

class ItemProduct extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      product: null
    }
  }

  onPressAddProduct = (item) => {
    this.props.reduxAddProduct(item, item.seller);
    this.updateProps();
  }

  onPressDecProduct = (item) => {
    this.props.reduxDecProduct(item, item.seller);
    this.updateProps();
  }

  updateProps = () => {
    const item = this.props.item;
    const product = this.props.redux.products.filter(p => p.product._id === item._id);
    this.setState({ product: product[0] });
  }

  render(){
    const item = this.props.item;
    return (
      <Card style={styles.gridItem}>
        <Card.Cover style={{height: 150}} source={item.photo_url ? { uri: item.photo_url } : require('../../assets/product-default.jpg')} />
        <Card.Content style={{alignContent: 'center', alignItems: 'center'}}>
          <Title style={{textAlign: 'center'}} numberOfLines={1}>{item.name}</Title>
          <Text >{item.description}</Text>
          {/*<Text style={styles.discount}>Descuento: 10%</Text>*/}
          <Subheading style={styles.price}>Valor: {formatPrice(item.price)}</Subheading>
          <Paragraph numberOfLines={1}>{item.category.name}</Paragraph>
        </Card.Content>
        <Card.Actions style={{justifyContent: 'center'}}>
          {(!this.state.product || this.state.product.quantity === 0) ?
            <View>
              <ViewButtonAddShoppin onPress={() => this.onPressAddProduct(item)}/>
            </View>
            :
            <View style={{flexDirection: 'row', height: 30, marginTop: 8}}>
              <Text style={styles.counterButton} onPress={() => this.onPressAddProduct(item)}><Icon name="plus" color="#fff" size={30} /></Text>
              <Text style={styles.counterText}>{this.state.product ? this.state.product.quantity : 0}</Text>
              <Text style={styles.counterButton}  onPress={() => this.onPressDecProduct(item)}><Icon name="minus" color="#fff" size={30} /></Text>
            </View>
          }
        </Card.Actions>
    </Card>
    )
  }
  
}

class ProductListScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return{
        title:"¿QUIÉN VENDE?, "+ navigation.getParam('title'),
        headerStyle: {
          backgroundColor: theme.colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: () => (
            <ShoppinCartButton onPress={navigation.getParam('onPressShoppinCart')} />
        )
    }
  };

  constructor(props) {
    super(props);
  }

  componentDidMount(){
      this.props.navigation.setParams({ onPressShoppinCart: this.onPressShoppinCart })
  }

  onPressShoppinCart = () => {
    this.props.navigation.navigate('ShoppinCart');
  }

  onPressProduct = item => {
    this.props.navigation.navigate('Product', { item });
  };

  render() {
    const { navigation } = this.props;
    const { _id, photo_url } = navigation.getParam('categorie');
    return (
      <View style={{marginBottom: 20}}>
        {/*<Image style={styles.banner} source={{ uri: photo_url }} />*/}
        <Query query={FEATCH_PRODUCT_CATEGORIE} variables={{_id}} pollInterval={5000}>
          {({loading, error, data}) => {
            if(loading) return <Loader />;
            if(data.getProductsCategories){
              return (
                <FlatList 
                  data={data.getProductsCategories}
                  numColumns={2}
                  keyExtractor={(item, i) => i.toString()}
                  renderItem={({item}) => (
                    <View style={{flex: 1, marginLeft: -5}}>
                      <ItemProduct item={item} redux={this.props} reduxAddProduct={this.props.reduxAddProduct} reduxDecProduct={this.props.reduxDecProduct}   />
                    </View>
                  )}
                />
              )
            }else{
              return <Text style={{margin: 15, fontSize: 25, textAlign: 'center', fontWeight: 'bold'}}>
                  Sin datos que mostrar...
                </Text>
            }
          }}
        </Query>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.ShoppingCartReducer.products,
    seller: state.ShoppingCartReducer.seller,
    filter: state.ShoppingCartReducer.filter,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    reduxAddProduct: (product, seller) => dispatch(addProduct(product, seller)),
    reduxDecProduct: (product, seller) => dispatch(decProduct(product, seller)),
    reduxFiltProduct: (_id) => dispatch(filtProduct(_id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductListScreen);