import { StyleSheet, Dimensions } from 'react-native';
import { theme } from '../../theme';
// screen sizing
const { width, height } = Dimensions.get('window');
// orientation must fixed
const SCREEN_WIDTH = width < height ? width : height;

const recipeNumColums = 2;
// item size
const RECIPE_ITEM_HEIGHT = 150;
const RECIPE_ITEM_MARGIN = 20;

// 2 photos per width
const styles = StyleSheet.create({
    counterButton: {
        backgroundColor: theme.colors.primary,
        width: 42, 
        textAlign: 'center', 
        borderRadius: 5, height: 25
    },
    counterText: {
        fontFamily: 'System',
        fontSize: 28,
        fontWeight: '400',
        marginTop: -4,
        color: '#000',
        marginLeft: 8,
        marginRight: 8
      },
    buttonText: {
        fontFamily: 'System',
        fontSize: 15,
        fontWeight: '300',
        color: '#007AFF',
        marginLeft: 40,
        marginRight: 40,
      },
    counterContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 20,
    },
    banner: {
        width: SCREEN_WIDTH - 30,
        height: 150,
        marginLeft: 15,
        marginTop: 15,
        marginBottom: 10,
        borderRadius: 15
    },
    discount: {
        color: 'red',
    },
    price : {
        color: 'blue'
    },
    gridItem: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#fff',
        alignItems: 'center',
        marginLeft: RECIPE_ITEM_MARGIN,
        marginTop: 20,
        marginRight: RECIPE_ITEM_MARGIN,
        width: 165,
        borderColor: '#cccccc',
        borderWidth: 0.5,
    },
    photo: {
        width: '100%',
        height: RECIPE_ITEM_HEIGHT,
        borderRadius: 15,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0
    },
    title: {
        flex: 1,
        fontSize: 17,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#444444',
        marginTop: 5,
        marginRight: 5,
        marginLeft: 5,
    },
    category: {
        marginTop: 5,
        marginBottom: 5
    }
});
export default styles;