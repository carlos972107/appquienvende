import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import {
    Avatar,
    Title,
    Caption,
    Paragraph,
    Drawer,
    Text
} from 'react-native-paper';
import {
    DrawerItem
} from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Query } from '@apollo/react-components';
import { FEATCH_USER } from '../../query';

function DrawerContainer(props) {
    return(
        <View style={{
          flex: 1,
          alignItems: 'flex-start',
        }}>
          <View style={{top: 50}}>
            <View style={styles.drawerContent}>
              <View style={styles.userInfoSection}>
                <Query query={FEATCH_USER} pollInterval={1000000}>
                  {({loading, error, data}) => {
                    if(loading) return <Text>Cargando...</Text>
                    if(data.getBuyer){
                      const item = data.getBuyer;
                      return (<View style={{flexDirection:'row',marginTop: 15, width: 190}}>
                        <Avatar.Image 
                          source={item.photo_url ? {
                              uri:  item.photo_url
                          } : require('../../assets/users_default.png')}
                          size={65}
                          style={{backgroundColor: 'transparent'}}
                        />
                        <View style={{marginLeft:15, flexDirection:'column'}}>
                          <Title style={styles.title}>{item.name + ' ' + item.lastname}</Title>
                          <Caption style={styles.caption}>{item.email}</Caption>
                          <View style={[styles.row]}>
                            <View style={styles.section}>
                              <Paragraph style={[styles.paragraph, styles.caption]}>5</Paragraph>
                              <Caption style={styles.caption}>Ranking</Caption>
                            </View>
                          </View>
                        </View>
                      </View>)
                    }else{
                      return (<Text>Sin datos que mostrar... </Text>)
                    }
                  }}
                </Query>
              </View>
              <Drawer.Section style={styles.drawerSection}>
                <DrawerItem 
                  icon={({color, size}) => (
                    <Icon 
                      name="home-outline" 
                      color={color}
                      size={size}
                    />
                  )}
                  label="Tiendas"
                  onPress={() => { props.navigation.navigate('Seller') }}
                />
                <DrawerItem 
                  icon={({color, size}) => (
                    <Icon 
                      name="clipboard-outline" 
                      color={color}
                      size={size}
                    />
                  )}
                  label="Pedidos"
                  onPress={() => {props.navigation.navigate('RequestOrders')}}
                />
                {/*<DrawerItem 
                  icon={({color, size}) => (
                    <Icon 
                      name="shopping-search" 
                      color={color}
                      size={size}
                    />
                  )}
                  label="Buscar"
                  onPress={() => {props.navigation.navigate('Search')}}
                  />*/}
                <DrawerItem 
                  icon={({color, size}) => (
                    <Icon 
                      name="account-circle-outline" 
                      color={color}
                      size={size}
                    />
                  )}
                  label="Perfil"
                  onPress={() => {props.navigation.navigate('Profile')}}
                />
                {/*<DrawerItem 
                  icon={({color, size}) => (
                    <Icon 
                      name="settings-outline" 
                      color={color}
                      size={size}
                    />
                  )}
                  label="Configuracion"
                  onPress={() => {props.navigation.navigate('Settings')}}
                  />*/}
                <DrawerItem 
                  icon={({color, size}) => (
                    <Icon 
                      name="security" 
                      color={color}
                      size={size}
                    />
                  )}
                  label="Cambiar contraseña"
                  onPress={() => {props.navigation.navigate('ChangePassword')}}
                />
              </Drawer.Section>
              <Drawer.Section style={styles.bottomDrawerSection}>
                <DrawerItem 
                  icon={({color, size}) => (
                    <Icon 
                      name="exit-to-app" 
                      color={color}
                      size={size}
                    />
                  )}
                  label="Cerrar sesion"
                  onPress={() => { 
                    localStorage.removeItem('mobile-token-authorization'); 
                    props.navigation.navigate('Login');
                  }}
                />
              </Drawer.Section>
            </View>
          </View>
        </View>
    );
}

DrawerContainer.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired
  })
};

export default DrawerContainer;

const styles = StyleSheet.create({
    drawerContent: {
      flex: 1,
    },
    userInfoSection: {
      paddingLeft: 20,
    },
    title: {
      fontSize: 16,
      marginTop: 3,
      fontWeight: 'bold',
    },
    caption: {
      fontSize: 14,
      lineHeight: 14,
    },
    row: {
      marginTop: 5,
      flexDirection: 'row',
      alignItems: 'center',
    },
    section: {
      flexDirection: 'row',
      alignItems: 'center',
      marginRight: 15,
    },
    paragraph: {
      fontWeight: 'bold',
      marginRight: 3,
    },
    drawerSection: {
      marginTop: 15,
      left: 25
    },
    bottomDrawerSection: {
        marginBottom: 15,
        left: 25
    },
    preference: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingVertical: 12,
      paddingHorizontal: 16,
    },
  });