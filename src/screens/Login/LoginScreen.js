import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Alert, ToastAndroid } from 'react-native';
import { TextInput as Input, Button as PaperButton } from 'react-native-paper';
import { useMutation, useLazyQuery } from '@apollo/react-hooks';
import { connect } from 'react-redux';
import { addSession } from '../../redux/actions';
import { LOGIN_BUYER } from '../../mutation';
import { FEATCH_USER } from '../../query';
import ContentRoot from '../../components/ContentRoot/ContentRoot';
import { theme } from '../../theme';
import Loader from '../../components/Loader/Loader';

const LoginScreen = ({ navigation, reduxAddSession }) => {

    const [email, setEmail] = React.useState({ value: '', error: '' });
    const [password, setPassword] = React.useState({ value: '', error: '' });
    const [loader, setLoader] = React.useState({ value: false });

    const [loginBuyer, { data }] = useMutation(LOGIN_BUYER);

    const [getBuyer] = useLazyQuery(FEATCH_USER, {
        onCompleted: data => {
            const { getBuyer } = data;
            reduxAddSession(getBuyer);
        }
    });

    React.useEffect(() => {
        ToastAndroid.show('Develope by Carlos Sanchez', ToastAndroid.BOTTOM);
    });

    const loginHandle = async () => {
        setLoader({ value: true });
        const validationEmail = emailValidator(email.value);
        const validationPassword = passwordValidator(password.value);
        if(validationEmail || validationPassword){
            setEmail({ ...email, error: validationEmail });
            setPassword({ ...password, error: validationPassword });
            setLoader({ value: false });
            return;
        }
        let { data } = await loginBuyer({ variables: { email: email.value, password: password.value }})
        .catch(error => {
            const err = error.graphQLErrors.map(error => error.message);
            setLoader({ value: false });
            Alert.alert('Credenciales incorrectas', err[0], [
                {text: 'Ok'}
            ]);
            return;
        });
        if (data.loginBuyer) {
            try{
                await getBuyer({});
                setLoader({ value: false });
                localStorage.setItem('mobile-token-authorization', data.loginBuyer);
                navigation.navigate('Seller');
                return;
            }catch(e){}
        }
    }

    const emailValidator = (email) => {
        const re = /\S+@\S+\.\S+/;
        if (!email || email.length <= 0) return 'El campo no puede quedar vacio.';
        if (!re.test(email)) return 'Correo electronico no valido.';
        return '';
    };

    const passwordValidator = (password) => {
        if (!password || password.length <= 0) return 'Contraseña no puede estar vacio.';
        return '';
    };

    return (
        <ContentRoot textHeader='Bienvenido a ¿Quién Vende?'>
            {loader.value && 
                <Loader />
            }
            <View style={styles.containerInput}>
                <Input
                    style={styles.input}
                    selectionColor={''}
                    theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                    mode="outlined"
                    label="Correo electronico"
                    returnKeyType="next"
                    value={email.value}
                    onChangeText={text => setEmail({ value: text, error: '' })}
                    error={!!email.error}
                    errorText={email.error}
                    autoCapitalize="none"
                    autoCompleteType="email"
                    textContentType="emailAddress"
                    keyboardType="email-address"
                    disabled={loader.value}
                />
                {email.error ? <Text style={styles.error}>{email.error}</Text> : null}
            </View>
            <View style={styles.containerInput}>
                <Input
                    style={styles.input}
                    selectionColor={''}
                    theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                    mode="outlined"
                    label="Contraseña"
                    returnKeyType="done"
                    value={password.value}
                    onChangeText={text => setPassword({ value: text, error: '' })}
                    error={!!email.error}
                    errorText={email.error}
                    secureTextEntry
                    disabled={loader.value}
                />
                {password.error ? <Text style={styles.error}>{password.error}</Text> : null}
            </View>
            <View style={styles.forgotPassword}>
                <TouchableOpacity onPress={() => navigation.navigate('ForgotPassword')} disabled={loader.value}>
                    <Text style={styles.label}>Olvidaste tu contraseña?</Text>
                </TouchableOpacity>
            </View>
            <PaperButton
                style={[
                    {
                        width: '100%',
                        marginVertical: 10,
                        backgroundColor: theme.colors.primary
                    }
                ]}
                labelStyle={styles.text}
                mode='contained'
                onPress={() => loginHandle()}
                disabled={loader.value}
            >
                Ingresar
            </PaperButton>
            <View style={styles.row}>
                <Text style={styles.label}>No tienes una cuenta? </Text>
                <TouchableOpacity onPress={() => navigation.navigate('Register')} disabled={loader.value}>
                    <Text style={styles.link}>Crear cuenta.</Text>
                </TouchableOpacity>
            </View>
        </ContentRoot>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        reduxAddSession: (buyer) => dispatch(addSession(buyer)),
    }
}

LoginScreen.navigationOptions = {
    headerShown: false
}

export default connect(null, mapDispatchToProps)(LoginScreen);

const styles = StyleSheet.create({
    containerInput: {
        width: '100%',
        marginVertical: 12,
    },
    input: {
        backgroundColor: '#fff',
        borderColor: theme.colors.primary
    },
    error: {
        fontSize: 14,
        color: theme.colors.error,
        paddingHorizontal: 4,
        paddingTop: 4,
    },
    forgotPassword: {
        width: '100%',
        alignItems: 'flex-end',
        marginBottom: 24,
    },
    row: {
        flexDirection: 'row',
        marginTop: 4,
    },
    label: {
        color: theme.colors.secondary,
    },
    link: {
        fontWeight: 'bold',
        color: theme.colors.primary,
    },
});