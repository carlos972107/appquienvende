import React from 'react';
import {
  FlatList,
  Text,
  View
} from 'react-native';
import { Card, Title, Subheading } from 'react-native-paper';
import styles from './styles';
import ShoppinCartButton from '../../components/ShoppinCartButton/ShoppingCartButton';
import { FEATCH_CATEGORIES_SELLER } from '../../query';
import { Query } from '@apollo/react-components';
import Loader from '../../components/Loader/Loader';
import { theme } from '../../theme';

export default class CategoriesListScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "¿QUIEN VENDE?, " + navigation.getParam('title'),
      headerStyle: {
        backgroundColor: theme.colors.primary,
      },
      headerTintColor: '#fff',
      headerRight: () => (
        <ShoppinCartButton onPress={navigation.getParam('onPressShoppinCart')} />
      )
    };
  };

  constructor(props) {
    super(props);
  }

  onPressShoppinCart = () => {
    this.props.navigation.navigate('ShoppinCart');
  }

  componentDidMount(){
    this.props.navigation.setParams({ onPressShoppinCart: this.onPressShoppinCart });
  }

  onPressCategorie = item => {
    const categorie = item;
    const title = item.name;
    this.props.navigation.navigate('ProductList', { categorie, title });
  };

  renderCategories = ({ item }) => (
    <Card onPress={() => this.onPressCategorie(item)} style={{margin: 15}}>
      <Card.Cover source={{ uri: item.photo_url }} style={{height: 180}} />
      <Card.Content>
        <Title>{item.name}</Title>
        <Subheading>{item.description}</Subheading>
      </Card.Content>
    </Card>
  );

  render() {
    const { navigation } = this.props;
    const _id = navigation.getParam('sellerId');
    return (
      <View style={{marginBottom: 40}}>
        <Text style={{fontSize: 30, fontWeight: 'bold', marginBottom: 5, marginTop: 10, marginLeft: 15}}>Categorias</Text>
        <Query query={FEATCH_CATEGORIES_SELLER} variables={{_id}} pollInterval={5000}>
          {({loading, error, data}) => {
            if(loading) return <Loader />;
            if(typeof(data.getCategoriesSeller) !== 'undefined' && data.getCategoriesSeller.length){
              return <FlatList
                vertical
                showsVerticalScrollIndicator={false}
                data={data.getCategoriesSeller}
                renderItem={this.renderCategories}
                keyExtractor={item => `${item._id}`}
              />
            }
          }}
        </Query>
      </View>
    );
  }
}
